#  PHP Coding Guidelines

This style guide is mostly based on the standards that are currently prevalent in [PSR-1](https://www.php-fig.org/psr/psr-1/) and [PSR-2](https://www.php-fig.org/psr/psr-2/) coding standards, although some conventions may still be included or prohibited on a case-by-case basis.

## General

### [#](#php-files) Files

All PHP files must end with a non-blank line, terminated with a single line feed (LF).

```php
<?php

function hello ()
{
	echo "Hello world!";
}
// <- Here is a non-block line.
```

> **Note!** For Visual Studio Code, you may add these lines to your `settings.json`

```json
{
	"files.insertFinalNewline": true,
	"files.trimFinalNewlines": true
}
```

### [#](#php-lines) Lines

Lines should not be longer than 80 characters; lines longer than that should be split into multiple subsequent lines of no more than 80 characters each.

```php
$endpoint = $this->website
	. $this->module
	. $this->version
	. $this->method
	. $this->url_param
	. trim($url)
	. $this->strategy_param
	. $strategy;
```

There must not be trailing whitespace at the end of lines.  Blank lines may be added to improve readability and to indicate related blocks of code except where explicitly forbidden.

> **Note!** For Visual Studio Code, you may add this line to your `settings.json`

```json
{
	"files.trimTrailingWhitespace": true
}
```

### [#](#php-indentions) Indentions

Code must use an indent of 4 spaces for each indent level, and must not use tabs for indenting.

If you are using _Visual Studio Code_, you may add these lines to your `settings.json`.

```json
{
	"editor.insertSpaces":  false
}
```

### [#](#php-comments) Comments

Comments, or docblocks for class properties and methods is highly recommended.

```php
class Foo
{
	/**
	 * Checks if foo is bar.
	 *
	 * @var bool
	 */
	protected $bar = false;

	/**
	 * Perform baz action.
	 *
	 * @param  string $param1
	 * @param  int|bool $param2
	 * @param  mixed $param3
	 * @return \Vendor\Models\Baz
	 *
	 * @throws \Vendor\Exceptions\SomeException
	 */
	public function baz($param1, $param2, $param3)
	{
		// code
	}
}
```

> **Note!** For Visual Studio Code, you may install [PHP DocBlocker](https://marketplace.visualstudio.com/items?itemName=neilbrayfield.php-docblocker) extension to insert docblocks conveniently.

## Classes, Properties and Methods

The term “class” refers to all classes, interfaces, and traits.

### [#](#php-classes) Class Declaration

Classes must be declared in `StudlyCase`.

```php
class SomeClass
{
	// code
}
```

### [#](#php-extends-implements) Extends, Implements

The `extends` and `implements` keywords must be declared on the same line as the class name. Lists of `implements` and, in the case of interfaces, `extends` may be split across multiple lines, where each subsequent line is indented once and arranged alphabetically.

```php
class Foo extends Baz implements
	ArrayAccess,
	Countable,
	Serializable
{
	// code
}
```

### [#](#php-namespaces) Namespaces

Namespaces must be arranged alphabetically.

```php
use Vendor\Alpha;
use Vendor\Beta;
use Vendor\Charlie;

class Foo
{
	// code
}
```

If you are using _Visual Studio Code_, you may install [PHP Namespace Resolver](https://marketplace.visualstudio.com/items?itemName=MehediDracula.php-namespace-resolver) to expand and import namespaces quickly. Add this config to your `settings.json` after installation.

```json
{
	"namespaceResolver.sortAlphabetically":  true
}
```

### [#](#php-traits) Traits

The `use` keyword used inside the classes to implement traits must be declared on the next line after the opening brace. Each individual trait that is imported into a class must be included one-per-line and each inclusion must have its own `use` import statement. Traits must be arranged alphabetically.

```php
class Foo
{
	use FirstTrait;
	use SecondTrait;

	// code
}
```

### [#](#php-properties) Properties

Properties must be declared in `camelCase`. There must not be more than one property declared per statement.

```php
class Foo
{
	// Good
	public $baz = false;

	// Bad
	protected $alpha, $bravo;
}
```

Properties must also be defined before any method declarations.

```php
class Foo
{
	protected $bar;

	public function __construct()
	{
		// Good.
		$this->bar = 'Bar';

		// Bad. `baz` was never defined.
		$this->baz = 'Baz';
	}
}
```

### [#](#php-constants) Constants

Constants must be declared in all uppercase with underscore separators.

```php
class Foo
{
	const API_VERSION = '2.1.1';
}
```

### [#](#php-methods) Methods

Methods must be declared in `camelCase` with the exception of test functions where they should be declared in all lowercase with underscore separators.

The opening brace must go on its own line, and the closing brace must go on the next line following the body. There must not be a space after the opening parenthesis, and there must not be a space before the closing parenthesis.

```php
class Foo
{
	public function someMethod()
	{
		// code
	}
}

class FooTest extends TestCase
{
	public function it_does_what_it_is_told()
	{
		// test body
	}
}
```

#### Method Arguments

Method arguments with default values must go at the end of the argument list.

```php
public function someMethod($arg1, $arg2 = null)
{
	// code
}
```

Argument lists may be split across multiple lines, where each subsequent line is indented once. When doing so, the first item in the list may be on the next line, and there must be only one argument per line.

```php
public function aVeryLongMethodName(
	ClassTypeHint $arg1,
	&$arg2,
	array $arg3 = []
) {
	// code
}
```

#### Method Calls

When making a method or function call, there must not be a space between the method or function name and the opening parenthesis, there must not be a space after the opening parenthesis, and there must not be a space before the closing parenthesis.

In the argument list, there must not be a space before each comma, and there must be one space after each comma. Argument lists may be split across multiple lines, where each subsequent line is indented once. When doing so, the first item in the list must be on the next line, and there must be only one argument per line.

```php
bar();
$foo->bar($arg1);
Foo::bar($arg2, $arg3);

$foo->bar(
    $longArgument,
    $longerArgument,
    $muchLongerArgument
);
```

## Control Structures

The general style rules for control structures are as follows:

-   There must be one space after the control structure keyword
-   There must not be a space after the opening parenthesis
-   There must not be a space before the closing parenthesis
-   There must be one space between the closing parenthesis and the opening brace
-   The structure body must be indented once
-   The closing brace must be on the next line after the body

Expressions inside control structures may be split across multiple lines where each subsequent line is indented once, and there must be only one expression per line.

```php
if (
	$param1 === 1
	&& $param2 === false
	&& ! $param3 === 'wooba'
) {
	// code
}
```

When negating an expression, a whitespace must be added after the `!` symbol.

```php
if (! $expr) {
	// code
}
```

### [#](#php-if-elseif-else) if, elseif, else

An `if` structure looks like the following. Note that `else` and `elseif` are on the same line as the closing brace from the earlier body. The keyword `elseif` should be used instead of `else if` so that all control keywords look like single words.

```php
if ($expr1) {
	// if body
} elseif ($expr2) {
	// elseif body
} else {
	// else body
}
```

### [#](#php-switch-case) switch, case

A `switch` structure looks like the following. The `case` statement must be indented once from `switch`, and the `break` keyword (or other terminating keyword) must be indented at the same level as the `case` body.

```php
switch ($expr) {
    case 0:
        echo 'First case, with a break';
        break;
    case 1:
        echo 'Second case, return instead of break';
        return;
    default:
        echo 'Default case';
        break;
}
```

### [#](#php-while-do-while) while, do while

A `while` statement looks like the following.

```php
while ($expr) {
    // structure body
}
```

Similarly, a `do while` statement looks like the following.

```php
do {
    // structure body;
} while ($expr);
```

### [#](#php-for) for

A `for` statement looks like the following.

```php
for ($i = 0; $i < 10; $i++) {
    // for body
}
```

### [#](#php-foreach) foreach

A `foreach` statement looks like the following.

```php
foreach ($iterable as $key => $value) {
    // foreach body
}
```

### [#](#php-try-catch) try, catch

A `try catch` block looks like the following.

```php
try {
    // try body
} catch (FirstExceptionType $e) {
    // catch body
} catch (OtherExceptionType $e) {
    // catch body
}
```
